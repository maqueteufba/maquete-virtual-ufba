﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseMenuSceneCanela: MonoBehaviour {

	public GameObject fps;                             //para habilitar o controle do jogador
	public GameObject  pauseComponents, botaoResume;
    public Camera fpscam;                              //trocar de cameras
	public BuzufbaCanela buzufba;
    
	void Start () {
		fpscam.enabled = true;
        Play();
	}

	void Update () {
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (Time.timeScale == 1) {
				Pause ();
			}
			else if (Time.timeScale == 0)
				Resumir ();
		}
	}

	public void Pause(){
		Time.timeScale = 0;
		fps.GetComponent<FirstPersonController> ().enabled = false;
		pauseComponents.SetActive (true);
	}

	public void Play(){	
		hideAll ();
		buzufba.hideAll ();
		fps.GetComponent<FirstPersonController> ().enabled = true;
		Time.timeScale = 1;
	}

	public void Resumir(){
			Play ();
			hideAll ();
		Time.timeScale = 1;
	}

	public void hideAll(){
		pauseComponents.SetActive (false);
	}

	public void Panfleto(string predio){

		if (predio.Equals ("biblio")) {
			Debug.Log ("BIBLIOTECA!");
		} else if (predio.Equals ("facom")) {
			Debug.Log ("FACOM!");
		} else if (predio.Equals ("bio")) {
			Debug.Log ("BIOLOGIA!");
		} else if (predio.Equals ("paf1")) {
			Debug.Log ("PAVILHAO DE AULAS REITOR FELIPPE SERPA (PAF1)!");
		} else if (predio.Equals ("farm")) {
			Debug.Log ("FARMACIA!");
		} else if (predio.Equals ("qui")) {
			Debug.Log ("QUIMICA!");
		} else if (predio.Equals ("fis")) {
			Debug.Log ("FISICA!");
		} else if (predio.Equals ("geo")) {
			Debug.Log ("GEOCIENCIAS!");
		} else if (predio.Equals ("ppgau")) {
			Debug.Log ("PPGAU!");
		} else if (predio.Equals ("fau")) {
			Debug.Log ("FACULDADE DE ARQUITETURA!");
		} else if (predio.Equals ("ceab")) {
			Debug.Log ("CEAB!");
		} else
			Debug.Log("whut?");
	}
}
