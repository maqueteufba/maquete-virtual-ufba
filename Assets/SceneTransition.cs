﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
	
public class SceneTransition : MonoBehaviour {
	//name of the scene you want to load
	public string scene;
	public Color loadToColor = Color.white;
	//public string buttonPressed;
	public Button buttonPressed1, buttonPressed2;
	public static bool pressed = false;

	public Sprite sprite; 
	public Canvas logo; 
	public GameObject load1, load2;
	Button bnt1, bnt2; 

	void Start(){
		bnt1 = buttonPressed1.GetComponent<Button> ();
		bnt2 = buttonPressed2.GetComponent<Button> ();
		load1.SetActive (false);
		load2.SetActive (false);

		//if (bnt.name == "ondina")
			bnt1.onClick.AddListener(delegate() { setNameScene (1); SetFlag(); });	
		//else if(bnt.name == "eba")
			bnt2.onClick.AddListener(delegate() { setNameScene (2); SetFlag();});	
					
		//Invoke ("SetFlag", 1);
		//bnt.onClick.AddListener(SetFlag);
			
	}
						
	public void setNameScene(int bock){
		if (bock==1){
			if (bnt1.name == "ondina") {
				this.scene = "cena 1";
				return;
			} else if (bnt1.name == "Reitoria") {
				this.scene = "cena 2 - canela";	
				return;
			}
		}

		if (bock==2){
			if (bnt2.name == "eba") {
				this.scene = "eba";
				return;
			} else if (bnt2.name == "Reitoria") {
				this.scene = "cena 2 - canela";	
				return;
			}
		}
	}
	//em ondina -> op1 Reitoria op2 eba
	//no canela -> op1 Ondina op2 eba
	//em belas artes -> op1 ondina op2 reitoria

	public void showPhrase(GameObject loading1, GameObject loading2){
		switch (this.scene) {
		case "eba":
			loading2.SetActive (true);
			loading1.SetActive (false);
			break;
		case "cena 1":
			loading1.SetActive (true);
			loading2.SetActive (false);
			break;
		case "cena 2 - canela":
			loading1.SetActive (true);
			loading2.SetActive (false);
			break;
		}
	}

	public void showLogo(){
		GameObject newObject = new GameObject ("object");
		RectTransform rect = newObject.AddComponent<RectTransform> ();
		rect.sizeDelta = new Vector2 (300, 300);
		Image image = newObject.AddComponent<Image> ();
		image.sprite = sprite;
		newObject.transform.SetParent (logo.transform, false);
//		showPhrase ();
		//loading2.SetActive (true);
	}

	void SetFlag(){
		pressed = true;
	}

	// Update is called once per frame
	void Update () {
		if (pressed){
			Initiate.Fade(scene,loadToColor,0.5f);
			//print ("entrou");
			pressed = false;
		}
	}
}
