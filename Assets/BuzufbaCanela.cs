﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
//using System.Collections;

public class BuzufbaCanela : MonoBehaviour {

	public GameObject FPS;      //current position of FirstPersonController
	public GameObject canvas;   //Used to hideAll visual elements
	public GameObject botaoADM, botaoOndina, botaoReitoria, botaoEBA;//, carregaOndina, carregaEBA;
	public Collider c;
	public SceneTransition sceneTransition;
	public GameObject load1, load2;

	// Use this for initialization
	void Start(){
		hideAll();

		c = GetComponent<Collider>();
		//print(c.name);
		//Debug.Log("start");
	}

	public void Resumir(){
		hideAll ();
		Time.timeScale = 1;
	}


	//Triggered when collides with buzufba
	public void OnTriggerEnter(Collider c0){

		//print (c0.name);
		//print (c.name);
		//Debug.Log("entrou");
		Pause();
		//canvas.SetActive (true);
		DetectCollider ();
	}

	public void showLoads(){
		sceneTransition.showPhrase (load1, load2);	
	}

	public void DetectCollider(){
		if (c.name.ToString() == "TeleportReitoria") {
			botaoADM.SetActive (true);
			botaoOndina.SetActive (true);
			botaoReitoria.SetActive (false);
			botaoEBA.SetActive (false);
		} else if (c.name.ToString() == "TeleportADM") {
			botaoReitoria.SetActive (true);
			botaoADM.SetActive (false);
			botaoOndina.SetActive (false);
			botaoEBA.SetActive (true);
		}
	}

	public void Pause(){
		Time.timeScale = 0;
		FPS.GetComponent<FirstPersonController>().enabled = false;
	}

	public void Play(){
		FPS.GetComponent<FirstPersonController>().enabled = true;
		Time.timeScale = 1;
	}

	//Takes care of teletransporting FPC
	public void Teleport(Transform newPosition){
		FPS.transform.position = newPosition.position;
		//Debug.Log("teletransportou para " + newPosition.ToString());
		hideAll();
		Play();
	}

	public void hideAll(){
		botaoOndina.SetActive(false);
		botaoReitoria.SetActive (false);
		botaoADM.SetActive (false);
		botaoEBA.SetActive (false);
		load1.SetActive (false);
		load2.SetActive (false);
		//carregaOndina.SetActive (false);
		//carregaEBA.SetActive (false);
	}

	public void DebugANDO(){
		Debug.Log ("APERTOU!");
	}
}