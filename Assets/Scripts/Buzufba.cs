﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
//using System.Collections;


public class Buzufba : MonoBehaviour {

    public GameObject FPS;      //current position of FirstPersonController
    public GameObject canvas;   //Used to hideAll visual elements
	public GameObject botaoOndina, botaoFAU, botaoPoli, botaoSL, botaoReitoria, botaoEBA;
    public Collider c;

    // Use this for initialization
    void Start(){
		hideAll();

		c = GetComponent<Collider>();
		//obj.SetActive (false);
		//print(c.name);
		//Debug.Log("start");
    }
		
	public void Resumir(){
		hideAll ();
		Time.timeScale = 1;
	}


    //Triggered when collides with buzufba
	public void OnTriggerEnter(Collider c0){

		//print (c0.name);
		//print (c.name);
        //Debug.Log("entrou");
        Pause();
		canvas.SetActive (true);
		DetectCollider ();
    }

	public void DetectCollider(){
		if (c.name.ToString() == "TeleportManagerFAU") {
			botaoOndina.SetActive (true);
			botaoPoli.SetActive (true);
			botaoFAU.SetActive (false);
			botaoSL.SetActive (false);
			botaoReitoria.SetActive (false);
			botaoEBA.SetActive (false);
		} else if (c.name.ToString() == "TeleportManagerOndina") {
			botaoFAU.SetActive (true);
			botaoSL.SetActive (true);
			botaoOndina.SetActive (false);
			botaoPoli.SetActive (false);
			botaoReitoria.SetActive (false);
			botaoEBA.SetActive (false);
		} else if (c.name.ToString() == "TeleportManagerPoli") {
			botaoFAU.SetActive (false);
			botaoSL.SetActive (true);
			botaoPoli.SetActive (false);
			botaoOndina.SetActive (false);
			botaoReitoria.SetActive (true);
			botaoEBA.SetActive (false);
		} else if (c.name.ToString() == "TeleportManagerSaoL") {
			botaoEBA.SetActive (true);
			botaoPoli.SetActive (false);
			botaoOndina.SetActive (true);
			botaoSL.SetActive (false);
			botaoFAU.SetActive (false);
			botaoReitoria.SetActive (false);
		}
	}

    public void Pause(){
        Time.timeScale = 0;
        FPS.GetComponent<FirstPersonController>().enabled = false;
    }

    public void Play(){
        FPS.GetComponent<FirstPersonController>().enabled = true;
        Time.timeScale = 1;
    }

    //Takes care of teletransporting FPC
    public void Teleport(Transform newPosition){
        FPS.transform.position = newPosition.position;
        //Debug.Log("teletransportou para " + newPosition.ToString());
        hideAll();
        Play();
    }

    public void hideAll(){
        canvas.SetActive(false);
    }

	public void DebugANDO(){
		Debug.Log ("APERTOU!");
	}
}