﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using System.Collections.Generic;

public class PauseMenu : MonoBehaviour {

	public GameObject fps;                                                                          //para habilitar o controle do jogador
	public GameObject botoesPasseios, pauseComponents, primeiraTela, botaoResume, botoesIrPara;     //conjunto de botoes
	public GameObject butoespasseio1, butoespasseio2, butoespasseio3;                               //conjunto de botoes
	public GameObject buttonpas1, buttonpas2, buttonpas3;

	public Camera fpscam, cinema;                                        //trocar de cameras
	public GameObject DebugButton, ErroPasseio;
	public Buzufba buzufba;
	private Animator animator;                                           //ativar a animacao da camera cinema
	private enum State {ANIM, PLAY};
	private State activeState;

	Vector3 pos, roteuler;
    //Quaternion roteulerq;
    private bool saiu = false;

    void Start () {
		fpscam.enabled = true;
		cinema.enabled = false;
        //anicam.enabled = false;	
		activeState = State.PLAY;

        animator = cinema.GetComponent<Animator> ();
		animator.enabled = false;

		DebugButton.SetActive (false);

		ErroPasseio.SetActive (false);
		botoesPasseios.SetActive(false);
		botoesIrPara.SetActive(false);
        Play();
	}

	void Update () {

		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (Time.timeScale == 1) {
				Pause ();
			}
			else if (Time.timeScale == 0)
				Resumir ();
			}

		Vector3 c = new Vector3(0, -20, 0);
		if (saiu == true) {
			fps.transform.position = pos;
			fps.transform.eulerAngles = c;
			saiu = false;
		}
	}

	public void Pause(){
		Time.timeScale = 0;
		fps.GetComponent<FirstPersonController> ().enabled = false;
		primeiraTela.SetActive (true);
		pauseComponents.SetActive (true);
	}

	public void Play(){	
		if (activeState == State.PLAY) {
			hideAll ();
			buzufba.hideAll ();
			fps.GetComponent<FirstPersonController> ().enabled = true;
			Time.timeScale = 1;
		} else if (activeState == State.ANIM) {
			sairAnimacao ();
			Resumir ();
		}
	}
    
    public void Resumir(){
		if (activeState == State.PLAY)
			Play ();
		else if (activeState == State.ANIM)
			hideAll ();
		Time.timeScale = 1;
	}

	public void SegundaTela(){
		if (activeState != State.ANIM) {
			primeiraTela.SetActive (false);
			botoesIrPara.SetActive(false);
			botoesPasseios.SetActive (true);
		}
		else{
			ErroPasseio.SetActive (true);
			return;
		}
	}

	public void SegundaTela1(){
		if (activeState != State.ANIM) {
			primeiraTela.SetActive (false);
			botoesPasseios.SetActive (false);
			botoesIrPara.SetActive(true);
			buttonpas1.SetActive (true);
			buttonpas2.SetActive (true);
			buttonpas3.SetActive (true);
			hidePasseio1 ();
			hidePasseio2 ();
			hidePasseio3 ();
		}
		else{
			ErroPasseio.SetActive (true);
			return;
		}
	}

    public void ChangeAnim(string anim){
		activeState = State.ANIM;
		animator.enabled = true;

		if (anim.Equals ("PasseioInterno")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("poli")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("arqmod")) {
			animator.SetTrigger (anim);
//---------------------------------------------------------------
		} else if (anim.Equals ("paf_1")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("paf1-farmacia")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("farm-quim")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("quim-fis")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("fis-fisnuclear")) {
			animator.SetTrigger (anim);
//---------------------------------------------------------------
		} else 	if (anim.Equals ("facom")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("facom-newbib")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("newbib-geociencias")) {
			animator.SetTrigger (anim);

		} else if (anim.Equals ("geociencias-arq")) {
			animator.SetTrigger (anim);
//---------------------------------------------------------------
		} else if (anim.Equals ("letras")) {
			animator.SetTrigger (anim);
		} else if (anim.Equals ("letras-vet")) {
			animator.SetTrigger (anim);
		}

        hideAll ();
		Time.timeScale = 1;
		TrocaCamera();
	}

	public void showPasseio1(){
		buttonpas1.SetActive (false);
		buttonpas2.SetActive (false);
		buttonpas3.SetActive (false);
		butoespasseio1.SetActive (true);
	}
	public void showPasseio2(){
		buttonpas1.SetActive (false);
		buttonpas2.SetActive (false);
		buttonpas3.SetActive (false);
		butoespasseio2.SetActive (true);
	}

	public void showPasseio3(){
		buttonpas1.SetActive (false);
		buttonpas2.SetActive (false);
		buttonpas3.SetActive (false);
		butoespasseio3.SetActive (true);
	}

	public void hidePasseio1(){
		butoespasseio1.SetActive (false);
	}

	public void hidePasseio2(){
		butoespasseio2.SetActive (false);
	}

	public void hidePasseio3(){
		butoespasseio3.SetActive (false);
	}

	public void hideAll(){
		ErroPasseio.SetActive (false);

		pauseComponents.SetActive (false);
		primeiraTela.SetActive (false);
		botoesPasseios.SetActive (false);
		botoesIrPara.SetActive(false);
	}

	public void Panfleto(string predio){
		if (predio.Equals ("biblio")) {
			Debug.Log ("BIBLIOTECA!");
		} else if (predio.Equals ("facom")) {
			Debug.Log ("FACOM!");
		} else if (predio.Equals ("bio")) {
			Debug.Log ("BIOLOGIA!");
		} else if (predio.Equals ("paf1")) {
			Debug.Log ("PAVILHAO DE AULAS REITOR FELIPPE SERPA (PAF1)!");
		} else if (predio.Equals ("farm")) {
			Debug.Log ("FARMACIA!");
		} else if (predio.Equals ("qui")) {
			Debug.Log ("QUIMICA!");
		} else if (predio.Equals ("fis")) {
			Debug.Log ("FISICA!");
		} else if (predio.Equals ("geo")) {
			Debug.Log ("GEOCIENCIAS!");
		} else if (predio.Equals ("ppgau")) {
			Debug.Log ("PPGAU!");
		} else if (predio.Equals ("fau")) {
			Debug.Log ("FACULDADE DE ARQUITETURA!");
		} else if (predio.Equals ("ceab")) {
			Debug.Log ("CEAB!");
		} else
			Debug.Log("whut?");
	}

	public void sairAnimacao(){
		TrocaCamera();
        activeState = State.PLAY;
        animator.enabled = false;
        animator.SetTrigger("back");
        Pause();
	}
    
    public void sairAni(){
        TrocaCamera();
        
		pos = cinema.transform.position;
		roteuler = cinema.transform.eulerAngles;

		activeState = State.PLAY;
        animator.enabled = false;
        animator.SetTrigger("back");
        
		//print ("asas" + roteuler);

		saiu = true;
	
		//pos = cinema.transform.localPosition;
		//rot = cinema.transform.localRotation;
		//roteuler = cinema.transform.localEulerAngles;
		//float 
		//var rotation = Quaternion.Euler(20, 20, 20);


		//fps.transform.rotation = roteulerq;
		//fps.transform.rotation.ToAngleAxis(out roteuler, out axis);
		//fps.transform.Rotate(roteuler);
		//print (roteuler);

		//roteuler = fps.transform.Rotate(roteuler);
        
		hideAll();
        fps.GetComponent<FirstPersonController>().enabled = true;
		Time.timeScale = 1;

		//print (fps.transform.eulerAngles);
    }
    
    public void TrocaCamera(){
		fpscam.enabled = !fpscam.enabled;
		cinema.enabled = !cinema.enabled;
	}

	public void ShowDebug(){
		Debug.Log (activeState.ToString());
		Debug.Log (animator.GetCurrentAnimatorStateInfo(0).ToString());
	}
}
