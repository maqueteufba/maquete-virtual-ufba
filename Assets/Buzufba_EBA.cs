﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Buzufba_EBA : MonoBehaviour {

	public GameObject FPS;                          //current position of FirstPersonController
	public GameObject canvas;                       //Used to hideAll visual elements
	public GameObject botaoOndina, botaoReitoria;   //, carregaOndina, carregaReitoria;
	public Collider c;	
	public SceneTransition sceneTransition;
	public GameObject load1, load2;

	void Start () {
		hideAll();
		c = GetComponent<Collider>();
	}

	//Triggered when collides with buzufba
	public void OnTriggerEnter(Collider c0){
		Pause();
		DetectCollider ();
	}

	public void DetectCollider(){
		if (c.name.ToString() == "TeleportEBA") {
			botaoOndina.SetActive (true);
			botaoReitoria.SetActive (true);
		}
	}

	public void Pause(){
		Time.timeScale = 0;
		FPS.GetComponent<FirstPersonController>().enabled = false;
	}

	public void Play(){
		FPS.GetComponent<FirstPersonController>().enabled = true;
		Time.timeScale = 1;
		hideAll ();
	}

	//Takes care of teletransporting FPC
	public void Teleport(Transform newPosition){
		FPS.transform.position = newPosition.position;
		hideAll();
		Play();
	}

	public void hideAll(){
		botaoOndina.SetActive(false);
		botaoReitoria.SetActive (false);
		load1.SetActive (false);
		load2.SetActive (false);
	}

	public void showButtons(){
		botaoOndina.SetActive (true);
		botaoReitoria.SetActive (true);
	}

	public void showLoads(){
		sceneTransition.showPhrase (load1, load2);	
	}

	// Update is called once per frame
	void Update () {
	}
}
